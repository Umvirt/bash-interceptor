# UNIX-pipe Interceptor

This is prof-of-work concept which interupt "curl | bash" pipes until user command.

## How to use

### 1. Add a pipe interceptor

Add "tee %id% | .waiter.sh" in "curl | bash" pipe.

It will looks like this:

        "curl | tee %id% | .waiter.sh | bash"

Where %id% - is pipe id.

Note: *curl* arguments are omitted in order to simplify a perception.

### 2. Run pipe

Run pipe inside this directory.

Pipe execution will be interrupted on *.waiter.sh* script.

curl output will be stored on "%id%" file.  

A lock-file with pipe id (%id%) and waiter.sh pid (%pid%) "%id%.%pid%.lock" will be created.

### 3. Inspect a code

Open "%id%" file with your favorite editor.

### 4. Finish

#### 4.a Terminate a pipe execution

Just call SIGTERM or press Ctrl+C on terminal where pipe is executed to cancel code execution. 

#### 4.b Resume a pipe execution

Just create file "%pid%.unlock".

## Samples

* *curlbash.dirty* - dirty version, without intercepter.
* *curlbash* - safe version, with intercepter.



