PID=$$

#create lock file
touch "$1.$PID.lock"

#Endless loop
count=1
while [ $count -le 1 ]
do
sleep 1
#Waitig for a file 'unlock'
if [  -f "$PID.unlock" ]; then
#If file is found exit from the loop
count=$(( $count + 1 ))
rm "$PID.unlock"
fi
done

# Pass output to STDOUT

while read line
do
  echo "$line"
done < "${1:-/dev/stdin}"

#remove lock file
rm "$1.$PID.lock"